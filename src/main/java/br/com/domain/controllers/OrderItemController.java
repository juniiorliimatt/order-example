package br.com.domain.controllers;

import br.com.domain.controllers.dto.OrderItemDTO;
import br.com.domain.services.OrderItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/order-items")
@RequiredArgsConstructor
public class OrderItemController {

  private final OrderItemService service;

  @GetMapping("/")
  public ResponseEntity<Iterable<OrderItemDTO>> findAll() {
	List<OrderItemDTO> dtos = new ArrayList<>();
	service.findAll().forEach(orderItem -> dtos.add(OrderItemDTO.convertToDto(orderItem)));
	return ResponseEntity.ok().body(dtos);
  }

  @GetMapping("/{id}")
  public ResponseEntity<OrderItemDTO> findById(@PathVariable("id") UUID id) {
	var optional = service.findById(id);
	if(optional.isEmpty()) {
	  return ResponseEntity.notFound().build();
	}
	return ResponseEntity.ok().body(OrderItemDTO.convertToDto(optional.get()));
  }

  @PostMapping
  public ResponseEntity<OrderItemDTO> save(@RequestBody OrderItemDTO dto) {
	var orderItemDto = OrderItemDTO.convertToDto(service.save(OrderItemDTO.convertToEntity(dto)));
	URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
											  .path("/order-item/{id}")
											  .buildAndExpand(orderItemDto.getId())
											  .toUri();
	return ResponseEntity.created(location).body(orderItemDto);
  }

  @PutMapping
  public ResponseEntity<OrderItemDTO> update(@RequestBody OrderItemDTO dto) {
	var orderItemDto = OrderItemDTO.convertToDto(service.update(OrderItemDTO.convertToEntity(dto)));
	return ResponseEntity.ok().body(orderItemDto);
  }

  @DeleteMapping
  @GetMapping("/{id}")
  public ResponseEntity<OrderItemDTO> deleteById(@PathVariable("id") UUID id) {
	var optional = service.findById(id);
	if(optional.isEmpty()) {
	  return ResponseEntity.notFound().build();
	}
	return ResponseEntity.ok().body(OrderItemDTO.convertToDto(optional.get()));
  }

}
