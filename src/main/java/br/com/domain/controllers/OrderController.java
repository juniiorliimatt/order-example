package br.com.domain.controllers;

import br.com.domain.controllers.dto.OrderDTO;
import br.com.domain.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

  private final OrderService service;

  @GetMapping("/")
  public ResponseEntity<List<OrderDTO>> findAll() {
	List<OrderDTO> dtos = new ArrayList<>();
	service.findAll().forEach(order -> dtos.add(OrderDTO.convertToDto(order)));
	return ResponseEntity.ok().body(dtos);
  }

  @GetMapping("/{id}")
  public ResponseEntity<OrderDTO> findById(@PathVariable("id") UUID id) {
	var optional = service.findById(id);

	if(optional.isEmpty()) {
	  return ResponseEntity.notFound().build();
	}

	var dto = OrderDTO.convertToDto(optional.get());
	return ResponseEntity.ok().body(dto);
  }

  @PostMapping
  public ResponseEntity<OrderDTO> save(@RequestBody OrderDTO dto) {
	var orderDto = OrderDTO.convertToDto(service.save(OrderDTO.convertToEntity(dto)));
	URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
											  .path("/order/{id}")
											  .buildAndExpand(orderDto.getId())
											  .toUri();
	return ResponseEntity.created(location).body(orderDto);
  }

  @PutMapping
  public ResponseEntity<OrderDTO> update(@RequestBody OrderDTO dto) {
	return ResponseEntity.ok().body(OrderDTO.convertToDto(service.update(OrderDTO.convertToEntity(dto))));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> delete(@PathVariable("id") UUID id) {
	var optional = service.findById(id);
	if(optional.isEmpty()) {
	  return ResponseEntity.notFound().build();
	}
	service.deleteById(id);
	return ResponseEntity.ok().build();
  }

}
