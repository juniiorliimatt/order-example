package br.com.domain.controllers.dto;

import br.com.domain.entities.Order;
import br.com.domain.entities.OrderItem;
import br.com.domain.entities.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemDTO {

  private UUID id;
  private Integer quantity;
  private BigDecimal price;
  private Product product;
  private Order order;

  public static OrderItem convertToEntity(OrderItemDTO dto) {
	var orderItem = new OrderItem();
	BeanUtils.copyProperties(dto, orderItem);
	return orderItem;
  }

  public static OrderItemDTO convertToDto(OrderItem orderItem) {
	var dto = new OrderItemDTO();
	BeanUtils.copyProperties(orderItem, dto);
	return dto;
  }

}
