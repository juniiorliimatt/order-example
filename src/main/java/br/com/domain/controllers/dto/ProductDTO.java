package br.com.domain.controllers.dto;

import br.com.domain.entities.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {

  private UUID id;
  private String name;
  private BigDecimal price;

  public static Product convertToEntity(ProductDTO dto) {
	var product = new Product();
	BeanUtils.copyProperties(dto, product);
	return product;
  }

  public static ProductDTO convertToDto(Product product) {
	var dto = new ProductDTO();
	BeanUtils.copyProperties(product, dto);
	return dto;
  }

}
