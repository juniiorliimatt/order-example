package br.com.domain.controllers.dto;

import br.com.domain.entities.Client;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientDTO implements Serializable {

  private UUID id;
  private String name;
  private String email;

  public static Client convertToEntity(ClientDTO dto) {
	var client = new Client();
	BeanUtils.copyProperties(dto, client);
	return client;
  }

  public static ClientDTO convertToDTO(Client client) {
	var dto = new ClientDTO();
	BeanUtils.copyProperties(client, dto);
	return dto;
  }

}
