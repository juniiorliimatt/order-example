package br.com.domain.controllers.dto;

import br.com.domain.entities.Client;
import br.com.domain.entities.Order;
import br.com.domain.entities.OrderItem;
import br.com.domain.entities.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO implements Serializable {

  private UUID id;
  private Instant moment;
  private OrderStatus status;
  private Client client;
  private List<OrderItem> items = new ArrayList<>();

  public static Order convertToEntity(OrderDTO dto) {
	var order = new Order();
	BeanUtils.copyProperties(dto, order);
	return order;
  }

  public static OrderDTO convertToDto(Order order) {
	var dto = new OrderDTO();
	BeanUtils.copyProperties(order, dto);
	return dto;
  }

}
