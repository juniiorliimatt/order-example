package br.com.domain.controllers;

import br.com.domain.controllers.dto.ProductDTO;
import br.com.domain.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

  private final ProductService service;

  @GetMapping("/")
  public ResponseEntity<Iterable<ProductDTO>> findAll() {
	List<ProductDTO> dtos = new ArrayList<>();
	service.findAll().forEach(product -> dtos.add(ProductDTO.convertToDto(product)));
	return ResponseEntity.ok().body(dtos);
  }

  @GetMapping("/{id}")
  public ResponseEntity<ProductDTO> findById(@PathVariable("id") UUID id) {
	var optional = service.findById(id);
	if(optional.isEmpty()) {
	  return ResponseEntity.notFound().build();
	}
	var dto = ProductDTO.convertToDto(optional.get());
	return ResponseEntity.ok().body(dto);
  }

  @PostMapping
  public ResponseEntity<ProductDTO> save(@RequestBody ProductDTO dto) {
	var product = ProductDTO.convertToDto(service.save(ProductDTO.convertToEntity(dto)));
	URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
											  .path("/product/{id}")
											  .buildAndExpand(product.getId())
											  .toUri();
	return ResponseEntity.created(location).body(product);
  }

  @PutMapping
  public ResponseEntity<ProductDTO> update(@RequestBody ProductDTO dto) {
	var product = ProductDTO.convertToDto(service.update(ProductDTO.convertToEntity(dto)));
	return ResponseEntity.ok().body(product);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<ProductDTO> delete(@PathVariable("id") UUID id) {
	var optional = service.findById(id);
	if(optional.isEmpty()) {
	  return ResponseEntity.notFound().build();
	}
	service.deleteById(id);
	return ResponseEntity.ok().build();
  }

}
