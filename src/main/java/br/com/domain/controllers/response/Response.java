package br.com.domain.controllers.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@SuperBuilder
@JsonInclude(NON_NULL)
public class Response{

  private String timeStamp;
  private int statusCode;
  private HttpStatus status;
  private String reason;
  private String message;
  private String developMessage;
  private Iterable<?> data;

}
