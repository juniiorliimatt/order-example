package br.com.domain.controllers;

import br.com.domain.controllers.dto.ClientDTO;
import br.com.domain.services.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.UUID;

@RestController
@RequestMapping("/client")
@RequiredArgsConstructor
public class ClientController{

  private final ClientService service;

  @GetMapping
  public ResponseEntity<Iterable<ClientDTO>> findAll(){
	var dto = new ArrayList<ClientDTO>();
	service.findAll().forEach(client->dto.add(ClientDTO.convertToDTO(client)));
	return ResponseEntity.ok().body(dto);
  }

  @GetMapping("/{id}")
  public ResponseEntity<ClientDTO> findById(@PathVariable("id") UUID id){
	var data = service.findById(id);
	if(data.isEmpty()){
	  return ResponseEntity.notFound().build();
	}
	var dto = ClientDTO.convertToDTO(data.get());
	return ResponseEntity.ok().body(dto);
  }

  @PostMapping
  public ResponseEntity<ClientDTO> save(@RequestBody ClientDTO dto){
	var clientDto = ClientDTO.convertToDTO(service.save(ClientDTO.convertToEntity(dto)));
	URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
											  .path("/client/{id}")
											  .buildAndExpand(clientDto.getId())
											  .toUri();

	return ResponseEntity.created(location).body(clientDto);
  }

  @PutMapping
  public ResponseEntity<ClientDTO> update(@RequestBody ClientDTO dto){
	return ResponseEntity.ok().body(ClientDTO.convertToDTO(service.update(ClientDTO.convertToEntity(dto))));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deleteById(@PathVariable("id") UUID id){
	var client = service.findById(id);
	if(client.isEmpty()){
	  return ResponseEntity.notFound().build();
	}
	service.deleteById(id);
	return ResponseEntity.ok().build();
  }

}
