package br.com.domain.services;

import br.com.domain.entities.Order;

import java.util.Optional;
import java.util.UUID;

public interface OrderService{

  Optional<Order> findById(UUID id);

  Iterable<Order> findAll();

  Order save(Order order);

  Order update(Order order);

  void deleteById(UUID id);

}
