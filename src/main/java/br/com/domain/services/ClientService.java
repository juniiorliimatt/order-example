package br.com.domain.services;

import br.com.domain.entities.Client;

import java.util.Optional;
import java.util.UUID;

public interface ClientService{

  Optional<Client> findById(UUID id);

  Iterable<Client> findAll();

  Client save(Client order);

  Client update(Client order);

  void deleteById(UUID id);

}
