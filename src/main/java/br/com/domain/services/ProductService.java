package br.com.domain.services;

import br.com.domain.entities.Product;

import java.util.Optional;
import java.util.UUID;

public interface ProductService{

  Optional<Product> findById(UUID id);

  Iterable<Product> findAll();

  Product save(Product order);

  Product update(Product order);

  void deleteById(UUID id);

}
