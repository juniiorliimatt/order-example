package br.com.domain.services;

import br.com.domain.entities.Product;
import br.com.domain.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService{

  private final ProductRepository repository;

  @Override
  public Optional<Product> findById(UUID id){
	return repository.findById(id);
  }

  @Override
  public Iterable<Product> findAll(){
	return repository.findAll();
  }

  @Override
  public Product save(Product order){
	return repository.save(order);
  }

  @Override
  public Product update(Product order){
	return repository.save(order);
  }

  @Override
  public void deleteById(UUID id){
	repository.deleteById(id);
  }

}
