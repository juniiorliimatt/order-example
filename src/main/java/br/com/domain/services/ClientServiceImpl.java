package br.com.domain.services;

import br.com.domain.entities.Client;
import br.com.domain.repositories.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService{

  private final ClientRepository repository;

  @Override
  public Optional<Client> findById(UUID id){
	return repository.findById(id);
  }

  @Override
  public Iterable<Client> findAll(){
	return repository.findAll();
  }

  @Override
  public Client save(Client order){
	return repository.save(order);
  }

  @Override
  public Client update(Client order){
	return repository.save(order);
  }

  @Override
  public void deleteById(UUID id){
	repository.deleteById(id);
  }

}
