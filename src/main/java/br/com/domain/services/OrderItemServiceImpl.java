package br.com.domain.services;

import br.com.domain.entities.OrderItem;
import br.com.domain.repositories.OrderItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OrderItemServiceImpl implements OrderItemService {

  private final OrderItemRepository repository;

  @Override
  public Optional<OrderItem> findById(UUID id) {
	return repository.findById(id);
  }

  @Override
  public Iterable<OrderItem> findAll() {
	return repository.findAll();
  }

  @Override
  public OrderItem save(OrderItem order) {
	return repository.save(order);
  }

  @Override
  public OrderItem update(OrderItem order) {
	return repository.save(order);
  }

  @Override
  public void deleteById(UUID id) {
	repository.deleteById(id);
  }

}
