package br.com.domain.services;

import br.com.domain.entities.Order;
import br.com.domain.repositories.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

  private final OrderRepository repository;

  @Override
  public Optional<Order> findById(UUID id) {
	return repository.findById(id);
  }

  @Override
  public Iterable<Order> findAll() {
	return repository.findAll();
  }

  @Override
  public Order save(Order order) {
	order.setMoment(Instant.now());
	return repository.save(order);
  }

  @Override
  public Order update(Order order) {
	return repository.save(order);
  }

  @Override
  public void deleteById(UUID id) {
	repository.deleteById(id);
  }

}
