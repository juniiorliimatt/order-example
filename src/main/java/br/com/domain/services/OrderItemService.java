package br.com.domain.services;

import br.com.domain.entities.OrderItem;

import java.util.Optional;
import java.util.UUID;

public interface OrderItemService {

  Optional<OrderItem> findById(UUID id);

  Iterable<OrderItem> findAll();

  OrderItem save(OrderItem order);

  OrderItem update(OrderItem order);

  void deleteById(UUID id);

}
