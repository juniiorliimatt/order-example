package br.com.domain.entities;

public enum OrderStatus{
  WAITING, PAID, DELIVERED, CANCELED
}
