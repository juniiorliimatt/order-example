package br.com.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;
  private Instant moment;
  private OrderStatus status;

  @ManyToOne
  @JoinColumn(name = "client_id")
  private Client client;

  @OneToMany(mappedBy = "order")
  private List<OrderItem> items = new ArrayList<>();

  public BigDecimal getTotal() {
	var total = BigDecimal.ZERO;
	for(OrderItem item : items) {
	  total = total.add(item.getSubTotal());
	}
	return total;
  }

}
